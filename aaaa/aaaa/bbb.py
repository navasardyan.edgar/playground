from django.http import HttpResponse
from django.views.generic import TemplateView


class MyFirstView(TemplateView):
    template_name = "about.html"
