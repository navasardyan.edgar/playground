from django.urls import include, path

from django.contrib import admin
from django.urls import path

from aaaa.bbb import MyFirstView
from aaaa.bbb import MyJsonView

urlpatterns = [
    path('hello_html', MyFirstView.as_view(), name='hello_html'),
    path('hello_json', MyJsonView.as_view(), name='hello_json')
]
